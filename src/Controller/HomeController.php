<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function admin()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'Admin',
        ]);
    }
    /**
     * @Route("/admin/home", name="admin_home")
     */
    public function adminHome()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'AdminHome',
        ]);
    }
}
